FROM python:3.5.9-buster AS linkchecker

ADD requirements.txt .
RUN pip install -r requirements.txt
ADD ./linkchecker .

CMD [ "python3", "link_check.py" ]
