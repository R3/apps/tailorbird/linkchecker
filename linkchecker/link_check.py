"""
Checks if the links are accessible
Requires: `beautifulsoup4`, `httplib2`
"""

import httplib2
import os

from bs4 import BeautifulSoup


class HtmlFilesDiscoverer:
    """Discovers html files in the specified directory."""

    def __init__(self, directory):
        self.directory = directory

    def _is_html_file(directory, filename):
        return filename.endswith('.html')

    def discover_files(self, filter_func=_is_html_file):
        files = []
        for (dir_path, _, file_names) in os.walk(self.directory):
            for filename in file_names:
                if filter_func(dir_path, filename):
                    files.append(os.path.join(dir_path, filename))
        return files


class HtmlLinkPicker:
    def __init__(self, file, whitelist=None, parser_type=None):
        self.file = file
        self.parser_type = 'html.parser' if parser_type is None else parser_type
        self.whitelist = whitelist if whitelist is not None else []

    def should_include_link(self, el):
        return not el.startswith('#') and \
               not el.startswith('mailto:') and \
               not el.startswith('ftp://') and \
               not el in self.whitelist

    def process_file(self):
        links = []
        with open(self.file, 'r', encoding="utf8") as f:
            contents = f.read()
            parser = BeautifulSoup(contents, self.parser_type)
            for link in parser.findAll('a'):
                link_address = link.get('href')
                if link_address is not None:
                    if self.should_include_link(link_address) and link_address not in links:
                        links.append(link_address)

        return links


class AccessibilityChecker:
    def __init__(self, directory, page_root, silent=None):
        self.h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
        self.directory = directory
        self.page_root = page_root
        self.silent = False if silent is None else silent

    def check(self, path):
        relativeLink = False
        if path[0] == '/':
            relativeLink = True
            path = self.page_root + path

        try:
            headers = {"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:50.0) Gecko/20100101 Firefox/50.0"}
            response = self.h.request(path, method='GET', headers=headers)

            if response[0].status == 200 or response[0].status == 999:
                if relativeLink:
                    status = ' 🟡 '
                else:
                    status = ' ✅ '

            print(status + path)
            return True
        except:
            return False

class LinkChecker:
    def __init__(self, directory, page_name, whitelist=None, silent=None):
        self.files = {}
        self.links = {}
        self.silent = False if silent is None else silent
        self.whitelist = [] if whitelist is None else whitelist
        self.discoverer = HtmlFilesDiscoverer(directory)
        self.checker = AccessibilityChecker(directory, page_name, self.silent)
        self.page_name = page_name

    def check(self):
        self.collect_links()
        if not self.silent:
            print(" > The following links will be checked:")
            print('\n'.join(sorted(self.links)))
            print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
            print("* Legend:")
            print("\t✅ Accessible links")
            print("\t🟡 Relative links with base URL: " + self.page_name)
            print("\t❌ Broken link")
            print("")
        return self.check_accessibility()

    def collect_links(self):
        def append_links(links, the_file):
            for link in links:
                if link in self.links:
                    self.links[link].append(the_file)
                else:
                    self.links[link] = [the_file]

        self.files = self.discoverer.discover_files()
        for file in self.files:
            new_links = HtmlLinkPicker(file, self.whitelist).process_file()
            append_links(new_links, file)

    def check_accessibility(self):
        found_bad_link = False
        for link in sorted(self.links):
            result = self.checker.check(link)
            if result is False:
                found_bad_link = True
                if not self.silent:
                    status = ' ❌ '
                    print(status + link)
                    print('List of files that contain this broken link:')
                    print('\n'.join(sorted(self.links[link])))
                    print('\n')
        return found_bad_link

def readWhitelist():
    try:
        with open("./check/whitelist.txt") as wlist:
            whitelist = [line.rstrip() for line in wlist]

        if len(whitelist) > 0:
            print(" > The whitelisted links are:")
            print('\n'.join(sorted(whitelist)))
            print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    except NameError:
        print ("NameError: name not defined")
    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        print(template.format(type(ex).__name__, ex.args))

    return whitelist

# determine the baseURL from the canonical URL in the Jekyll generated page
with open('./check/index.html', 'r', encoding="utf8") as f:
    contents = f.read()
parser = BeautifulSoup(contents, features="html.parser")
canonical = parser.findAll('link', {"rel": "canonical"})
baseURL = canonical[0].get('href')

whitelist = readWhitelist()
checker = LinkChecker('./check/', baseURL, whitelist)
if checker.check():
    exit(1)
exit(0)
